﻿using Unity.Entities;

namespace Game
{
    public partial class DestroyAfterTimeSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;

            Entities
                .WithAll<DestroyAfterTimeComponentData>()
                .ForEach((Entity entity, ref DestroyAfterTimeComponentData destroyAfterTime) =>
                {
                    destroyAfterTime.remainingTime -= deltaTime;
                    if (destroyAfterTime.remainingTime <= 0)
                    {
                        EntityManager.DestroyEntity(entity);
                    }
                }).WithStructuralChanges().Run();
        }
    }
}
