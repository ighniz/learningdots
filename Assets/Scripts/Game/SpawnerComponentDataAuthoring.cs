﻿using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct SpawnerComponentData : IComponentData
    {
        public Entity prefab;
        public float remainingCooldown;
        public float cooldown;
        public bool isSpawning;
    }

    public class SpawnerComponentDataAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        public GameObject prefab;
        public float cooldown;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new SpawnerComponentData
            {
                prefab = conversionSystem.GetPrimaryEntity(prefab),
                cooldown = cooldown
            });
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(prefab);
        }
    }
}