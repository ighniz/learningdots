﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    [GenerateAuthoringComponent]
    public struct KeyControllerComponentData : IComponentData
    {
        public KeyCode moveForward;
        public KeyCode moveBackward;
        public KeyCode moveLeft;
        public KeyCode moveRight;
        public KeyCode shoot;
        public KeyCode lookForward;
        public KeyCode lookBackward;
        public KeyCode lookLeft;
        public KeyCode lookRight;
    }
}