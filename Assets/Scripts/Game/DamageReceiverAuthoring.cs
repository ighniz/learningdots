﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct DamageReceiverComponentData : IComponentData
    {
        public float energy;
    }

    public class DamageReceiverAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float energy;
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new DamageReceiverComponentData
            {
                energy = energy
            });
        }
    }
}