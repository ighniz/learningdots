﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Game
{
    public partial class TriggersCheckerSystem : SystemBase
    {
        private StepPhysicsWorld stepPhysicsWorld;
        
        [BurstCompile]
        public struct TriggerJob : ITriggerEventsJob
        {
            public ComponentDataFromEntity<DamageReceiverComponentData> damageReceiverComponents;
            public ComponentDataFromEntity<BulletComponentData> bulletsData;

            public void Execute(TriggerEvent triggerEvent)
            {
                CheckBulletAndDamageReceiver(ref triggerEvent);
            }

            private void CheckBulletAndDamageReceiver(ref TriggerEvent triggerEvent)
            {
                DamageReceiverComponentData damageReceiver;
                BulletComponentData bullet;
                if (damageReceiverComponents.TryGetComponent(triggerEvent.EntityA, out damageReceiver) && bulletsData.TryGetComponent(triggerEvent.EntityB, out bullet) ||
                    damageReceiverComponents.TryGetComponent(triggerEvent.EntityB, out damageReceiver) && bulletsData.TryGetComponent(triggerEvent.EntityA, out bullet))
                {
                    if (!bullet.isDestroyed)
                    {
                        damageReceiver.energy -= bullet.damage;
                        bullet.isDestroyed = true;

                        if (damageReceiverComponents.HasComponent(triggerEvent.EntityA))
                        {
                            damageReceiverComponents[triggerEvent.EntityA] = damageReceiver;
                            bulletsData[triggerEvent.EntityB] = bullet;
                        }
                        else
                        {
                            damageReceiverComponents[triggerEvent.EntityB] = damageReceiver;
                            bulletsData[triggerEvent.EntityA] = bullet;
                        }
                    }
                }
            }
        }

        override protected void OnCreate()
        {
            stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        }

        override protected void OnUpdate()
        {
            var triggerJob = new TriggerJob
            {
                damageReceiverComponents = GetComponentDataFromEntity<DamageReceiverComponentData>(),
                bulletsData = GetComponentDataFromEntity<BulletComponentData>()
            };
            JobHandle jobHandle = triggerJob.Schedule(stepPhysicsWorld.Simulation, Dependency);
            jobHandle.Complete();
        }
    }
}