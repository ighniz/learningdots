﻿using Unity.Entities;

namespace Game
{
    public partial class DamageReceiverSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            Entities
                .WithAll<DamageReceiverComponentData>()
                .ForEach((ref DamageReceiverComponentData damageReceiver,
                          in Entity entity) =>
                {
                    if (damageReceiver.energy <= 0)
                    {
                        EntityManager.DestroyEntity(entity);
                    }
                }).WithStructuralChanges().Run();
        }
    }
}