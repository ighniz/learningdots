﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public partial class BulletSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;
            Entities
                .WithAll<BulletComponentData>()
                .ForEach((ref Translation translation,
                          ref Rotation rotation,
                          ref DestroyAfterTimeComponentData destroyAfterTime,
                          in BulletComponentData bulletComponentData) =>
                {
                    translation.Value += math.forward(rotation.Value) * bulletComponentData.speed * deltaTime;
                    if (bulletComponentData.isDestroyed)
                    {
                        destroyAfterTime.remainingTime = 0;
                    }
                }).ScheduleParallel();
        }
    }
}