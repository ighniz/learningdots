﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct SimpleEnemyBehaviourComponentData : IComponentData
    {
        public float movementSpeed;
    }

    public class SimpleEnemyBehaviourAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float movementSpeed;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new SimpleEnemyBehaviourComponentData
            {
                movementSpeed = movementSpeed
            });
        }
    }
}