﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct GunComponentData : IComponentData
    {
        public bool isShooting;
    }

    public struct GunSpawnPointBuffer : IBufferElementData
    {
        public Entity spawnPoint;
    }

    public class GunComponentDataAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponent<GunComponentData>(entity);
            DynamicBuffer<GunSpawnPointBuffer> spawnPointBuffer = dstManager.AddBuffer<GunSpawnPointBuffer>(entity);

            SpawnerComponentDataAuthoring[] spawners = GetComponentsInChildren<SpawnerComponentDataAuthoring>();
            foreach (SpawnerComponentDataAuthoring spawner in spawners)
            {
                spawnPointBuffer.Add(new GunSpawnPointBuffer { spawnPoint = conversionSystem.GetPrimaryEntity(spawner) });
            }
        }
    }
}