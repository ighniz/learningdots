﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct BulletComponentData : IComponentData
    {
        public int damage;
        public float speed;
        public bool isDestroyed;
    }

    public class BulletAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int damage;
        public float speed;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new BulletComponentData
            {
                damage = damage,
                speed = speed
            });
        }
    }
}