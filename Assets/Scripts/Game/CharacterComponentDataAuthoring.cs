﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Game
{
    public struct CharacterComponentData : IComponentData
    {
        public int energy;
        public int speed;
        public float3 movementDirection;
        public float3 lookDirection;
        public bool isShooting;
    }

    public struct CharacterGunBuffer : IBufferElementData
    {
        public Entity entity;
    }

    public class CharacterComponentDataAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int energy;
        public int speed;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new CharacterComponentData
            {
                energy = energy,
                speed = speed,
                lookDirection = new float3(0, 0, 1)
            });

            DynamicBuffer<CharacterGunBuffer> gunBuffer = dstManager.AddBuffer<CharacterGunBuffer>(entity);
            GunComponentDataAuthoring[] allGuns = GetComponentsInChildren<GunComponentDataAuthoring>();
            foreach (GunComponentDataAuthoring gunComponent in allGuns)
            {
                gunBuffer.Add(new CharacterGunBuffer { entity = conversionSystem.GetPrimaryEntity(gunComponent) });
            }
        }
    }
}