﻿using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public partial class SimpleEnemyBehaviourSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;

            EntityQuery mainCharacterQuery = GetEntityQuery(ComponentType.ReadOnly<KeyControllerComponentData>());
            Entity mainCharacter = mainCharacterQuery.ToEntityArray(Allocator.Temp).First();
            float3 mainCharacterPosition = GetComponent<LocalToWorld>(mainCharacter).Position;
            
            Entities
                .WithAll<SimpleEnemyBehaviourComponentData>()
                .ForEach((ref LocalToWorld localToWorld,
                          ref Translation translation,
                          ref SimpleEnemyBehaviourComponentData simpleEnemyBehaviour,
                          ref Rotation rotation) =>
                {
                    float3 mainCharacterDirection = math.normalize(mainCharacterPosition - localToWorld.Position);
                    translation.Value += mainCharacterDirection * simpleEnemyBehaviour.movementSpeed * deltaTime;
                    rotation.Value = quaternion.LookRotation(mainCharacterDirection, math.up());
                }).ScheduleParallel();
        }
    }
}