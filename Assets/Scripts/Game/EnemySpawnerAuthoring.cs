﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct EnemySpawnerComponentData : IComponentData
    {
        public float remainingTimeForFirstSpawn;
    }

    public class EnemySpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float firstSpawnDelay;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new EnemySpawnerComponentData
            {
                remainingTimeForFirstSpawn = firstSpawnDelay
            });
        }
    }
}