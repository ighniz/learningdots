﻿using Unity.Entities;

namespace Game
{
    public partial class GunSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            Entities
                .WithAll<GunComponentData>()
                .ForEach((
                    Entity entity,
                    ref GunComponentData gun) =>
                {
                    if (gun.isShooting)
                    {
                        if (GetBufferFromEntity<GunSpawnPointBuffer>().TryGetBuffer(entity, out DynamicBuffer<GunSpawnPointBuffer> gunSpawnPointBuffers))
                        {
                            foreach (GunSpawnPointBuffer gunBufferItem in gunSpawnPointBuffers)
                            {
                                var spawner = GetComponent<SpawnerComponentData>(gunBufferItem.spawnPoint);
                                spawner.isSpawning = true;
                                SetComponent(gunBufferItem.spawnPoint, spawner);
                            }
                        }
                    }
                    gun.isShooting = false;
                }).Schedule();
        }
    }
}