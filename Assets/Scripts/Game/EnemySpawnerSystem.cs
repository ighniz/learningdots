﻿using Unity.Entities;
using Unity.Mathematics;

namespace Game
{
    public partial class EnemySpawnerSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;
            
            Entities
                .WithAll<EnemySpawnerComponentData>()
                .ForEach((ref SpawnerComponentData spawner,
                          ref EnemySpawnerComponentData enemySpawner) =>
                {
                    enemySpawner.remainingTimeForFirstSpawn = math.max(0, enemySpawner.remainingTimeForFirstSpawn - deltaTime);
                    if (enemySpawner.remainingTimeForFirstSpawn <= 0)
                    {
                        spawner.isSpawning = true;
                    }
                }).ScheduleParallel();
        }
    }
}