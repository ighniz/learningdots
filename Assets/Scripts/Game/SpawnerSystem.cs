﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public partial class SpawnerSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;

            Entities
                .WithAll<SpawnerComponentData>()
                .ForEach((
                    ref LocalToWorld localToWorld,
                    ref SpawnerComponentData spawnerComponentData) =>
                {
                    spawnerComponentData.remainingCooldown = math.max(0, spawnerComponentData.remainingCooldown - deltaTime);
                    if (spawnerComponentData.remainingCooldown <= 0 && spawnerComponentData.isSpawning)
                    {
                        Entity newEntity = EntityManager.Instantiate(spawnerComponentData.prefab);
                        EntityManager.SetComponentData(newEntity, new Translation
                        {
                            Value = localToWorld.Position
                        });
                        EntityManager.SetComponentData(newEntity, new Rotation
                        {
                            Value = quaternion.LookRotation(localToWorld.Forward, math.up())
                        });

                        spawnerComponentData.remainingCooldown = spawnerComponentData.cooldown;
                    }
                    spawnerComponentData.isSpawning = false;
                }).WithStructuralChanges().Run();
        }

        /*
        public void Spawn(in SpawnerComponentData spawnerComponentData)
        {
            Entity newEntity = EntityManager.Instantiate(spawnerComponentData.prefab);

            var originData = GetComponent<LocalToWorld>(spawnerComponentData);
            EntityManager.SetComponentData(newEntity, new Translation
            {
                Value = originData.Position
            });
            EntityManager.SetComponentData(newEntity, new Rotation
            {
                Value = quaternion.LookRotation(originData.Forward, math.up())
            });
            
        }*/
    }
}