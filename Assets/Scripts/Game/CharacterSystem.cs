﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public partial class CharacterSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            UpdateCharacterBehaviour(Time.DeltaTime);
            CheckForShooting();
        }

        private void UpdateCharacterBehaviour(float deltaTime)
        {
            Entities
                .WithAll<CharacterComponentData>()
                .ForEach((ref Translation translation,
                    ref Rotation rotation,
                    ref CharacterComponentData character) =>
                {
                    //Movement.
                    translation.Value += character.movementDirection * character.speed * deltaTime;

                    //Look.
                    if (math.any(character.lookDirection))
                    {
                        quaternion targetRotation = quaternion.LookRotation(character.lookDirection, math.up());
                        rotation.Value = targetRotation;
                    }
                }).ScheduleParallel();
        }

        private void CheckForShooting()
        {
            Entities
                .WithAll<CharacterComponentData>()
                .ForEach((ref CharacterComponentData character,
                    in Entity entity) =>
                {
                    if (character.isShooting)
                    {
                        if (GetBufferFromEntity<CharacterGunBuffer>().TryGetBuffer(entity, out var gunBuffer))
                        {
                            foreach (CharacterGunBuffer gun in gunBuffer)
                            {
                                var gunComponentData = GetComponent<GunComponentData>(gun.entity);
                                gunComponentData.isShooting = true;
                                SetComponent(gun.entity, gunComponentData);
                            }
                        }
                    }
                    character.isShooting = false;
                }).Schedule();
        }
    }
}