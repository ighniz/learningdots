﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Game
{
    public partial class InputCharacterControllerSystem : SystemBase
    {
        override protected void OnUpdate()
        {
            Entities
                .WithAll<KeyControllerComponentData>()
                .ForEach((ref KeyControllerComponentData keyController,
                          ref CharacterComponentData character) =>
                {
                    UpdateMovement(ref keyController, ref character);
                    UpdateLook(ref keyController, ref character);

                    character.isShooting = Input.GetKey(keyController.shoot);
                }).WithoutBurst().Run();
        }

        private void UpdateLook(ref KeyControllerComponentData keyController, ref CharacterComponentData character)
        {
            if (Input.GetKey(keyController.lookForward))
            {
                character.lookDirection.z = 1;
            }
            else if (Input.GetKey(keyController.lookBackward))
            {
                character.lookDirection.z = -1;
            }
            else
            {
                character.lookDirection.z = 0;
            }
                    
            if (Input.GetKey(keyController.lookLeft))
            {
                character.lookDirection.x = -1;
            }
            else if (Input.GetKey(keyController.lookRight))
            {
                character.lookDirection.x = 1;
            }else
            {
                character.lookDirection.x = 0;
            }
        }

        private void UpdateMovement(ref KeyControllerComponentData keyController, ref CharacterComponentData character)
        {
            if (Input.GetKey(keyController.moveForward))
            {
                character.movementDirection.z = 1;
            }
            else if (Input.GetKey(keyController.moveBackward))
            {
                character.movementDirection.z = -1;
            }
            else
            {
                character.movementDirection.z = 0;
            }
                    
            if (Input.GetKey(keyController.moveLeft))
            {
                character.movementDirection.x = -1;
            }
            else if (Input.GetKey(keyController.moveRight))
            {
                character.movementDirection.x = 1;
            }
            else
            {
                character.movementDirection.x = 0;
            }
        }
    }
}