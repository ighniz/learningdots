﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public struct DestroyAfterTimeComponentData : IComponentData
    {
        public float remainingTime;
    }
    
    public class DestroyAfterTimeAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float timeToDestroy;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new DestroyAfterTimeComponentData{remainingTime = timeToDestroy});
        }
    }
}