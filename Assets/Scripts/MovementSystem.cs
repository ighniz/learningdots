/*
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public partial class MovementSystem : SystemBase
{
    override protected void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        const float speed = 15;
        
        Entities
            .WithAll<MovementComponentData>()
            .ForEach((ref Translation translation) =>
        {
            translation.Value += -math.normalize(translation.Value) * speed * deltaTime;
        }).WithBurst(FloatMode.Fast).ScheduleParallel();
    }
}
*/