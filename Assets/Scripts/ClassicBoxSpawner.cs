﻿using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using Random = UnityEngine.Random;

public class ClassicBoxSpawner : MonoBehaviour
{
    [SerializeField]
    private int amountOfBoxes;

    [SerializeField]
    private float movementSpeed;

    [SerializeField]
    private GameObject boxPrefab;

    [SerializeField]
    private float minPosition;
    
    [SerializeField]
    private float maxPosition;

    private TransformAccessArray boxes;
    private MoveToCenterJob moveToCenterJob;
    private JobHandle jobHandle;
    private Transform[] boxesArray;

    private void Start()
    {
        //Creamos el array que va a contener los transforms de los objetos que vamos a mover.
        boxes = new TransformAccessArray(amountOfBoxes, 100);
        boxesArray = new Transform[amountOfBoxes];
        
        for (var i=0; i<amountOfBoxes; i++)
        {
            GameObject box = Instantiate(boxPrefab);
            box.transform.position = new Vector3(
                Random.Range(minPosition, maxPosition), 
                Random.Range(minPosition, maxPosition), 
                Random.Range(minPosition, maxPosition));
            
            boxes.Add(box.transform);
            boxesArray[i] = box.transform;
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.F))
        {
            //Creamos el job y lo configuramos.
            MoveWithJobs();
        }else if (Input.GetKey(KeyCode.G))
        {
            //Creamos el job y lo configuramos.
            MoveWithClassicFor();
        }
    }

    private void MoveWithClassicFor()
    {
        for (var i = 0; i < boxesArray.Length; i++)
        {
            Vector3 currentPosition = boxesArray[i].position;
            Vector3 direction = -currentPosition.normalized;
            boxesArray[i].position += direction * Time.deltaTime * movementSpeed;
            boxesArray[i].rotation = Quaternion.LookRotation(direction);
        }
    }

    private void MoveWithJobs()
    {
        moveToCenterJob = new MoveToCenterJob
        {
            deltaTime = Time.deltaTime,
            speed = movementSpeed
        };

        jobHandle = moveToCenterJob.Schedule(boxes);
    }

    private void LateUpdate()
    {
        jobHandle.Complete();
    }

    private void OnDestroy()
    {
        boxes.Dispose();
    }
}