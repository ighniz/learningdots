﻿using Unity.Entities;
using Unity.Mathematics;

public struct RotateComponentData : IComponentData
{
    public float3 speed;
}