﻿using Unity.Burst;
using UnityEngine;
using UnityEngine.Jobs;

[BurstCompile]
public struct MoveToCenterJob : IJobParallelForTransform
{
    public float deltaTime;
    public float speed;
    
    public void Execute(int index, TransformAccess transform)
    {
        Vector3 currentPosition = transform.position;
        Vector3 direction = -currentPosition.normalized;
        transform.position += direction * deltaTime * speed;
        transform.rotation = Quaternion.LookRotation(direction);
    }
}