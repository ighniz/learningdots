/*
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public partial class EntitySpawnerSystem : SystemBase
{
    private Entity prefabEntity;
    private Random random;
    
    override protected void OnCreate()
    {
        Debug.Log("OnCreate");
    }

    override protected void OnStartRunning()
    {
        Debug.Log("OnStartRunning");
        
        //Creamos nuestro prefab entity 
        prefabEntity = AssetsHolder.GetEntity(AssetsHolder.AssetType.MainCharacter);
        
        //Creamos nuestro random.
        random = new Random();
        random.InitState();
    }

    private void SpawnEntity()
    {
        Entity newEntity = EntityManager.Instantiate(prefabEntity);

        const float max = 100;
        const float min = -100;
        
        EntityManager.SetComponentData(newEntity, new Translation
        {
            Value = random.NextFloat3() * (max - min) + min
        });
        EntityManager.AddComponent<MovementComponentData>(newEntity);
        EntityManager.AddComponent<RotateComponentData>(newEntity);
        EntityManager.AddComponent<DestroyAfterTimeComponentData>(newEntity);
        EntityManager.SetComponentData(newEntity, new DestroyAfterTimeComponentData
        {
            remainingTime = 10
        });
    }

    override protected void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < 30000; i++)
            {
                SpawnEntity();
            }
        }
    }
}
*/