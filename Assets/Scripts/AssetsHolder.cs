using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using Unity.Entities;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(AssetsHolder), menuName = nameof(AssetsHolder))]
public class AssetsHolder : ScriptableObject
{
    public enum AssetType
    {
        MainCharacter,
        Enemy,
        Bullet
    }

    static public AssetsHolder instance;
    static public AssetsHolder Instance {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<AssetsHolder>(nameof(AssetsHolder));
                instance.ConvertPrefabsToEntities();
            }

            return instance;
        }
    }
    
    [SerializeField]
    private SerializableDictionaryBase<AssetType, GameObject> assetsByType;
    private SerializableDictionaryBase<AssetType, Entity> entitiesByType;

    private void ConvertPrefabsToEntities()
    {
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        entitiesByType = new SerializableDictionaryBase<AssetType, Entity>();
        
        foreach (KeyValuePair<AssetType, GameObject> assetPair in assetsByType)
        {
            Entity newEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(assetPair.Value, settings);
            entitiesByType.Add(assetPair.Key, newEntity);
        }
    }

    static public Entity GetEntity(AssetType assetType)
    {
        return Instance.entitiesByType[assetType];
    }

    static public void RegisterEntity(Entity entity, AssetType assetType)
    {
        Instance.entitiesByType.Add(assetType, entity);
    }
}
