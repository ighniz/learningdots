﻿/*
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public partial class RotateSystem : SystemBase
{
    override protected void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        Entities
            .WithAll<RotateComponentData>()
            .ForEach((ref Rotation rotation) =>
            {
                float radians = math.radians(180 * deltaTime);
                rotation.Value = math.mul(rotation.Value, quaternion.RotateY(radians));
            })
            .Run();
    }
}
*/