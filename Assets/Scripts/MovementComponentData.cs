using Unity.Entities;
using Unity.Mathematics;

public struct MovementComponentData : IComponentData
{
    public float3 speed;
}
